package decraft;

import java.net.URLClassLoader;

import java.nio.file.Paths;

import java.util.HashMap;
import java.util.LinkedList;

import org.graalvm.polyglot.Engine;
import org.graalvm.polyglot.Value;

public class Decraft {
   
   public static Engine engine;
   public static final HashMap<String, LinkedList<Value>> events = new HashMap<>();
   public static final LinkedList<Hook> hooks = new LinkedList<>();
   public static final HashMap<String, URLClassLoader> loaders = new HashMap<>();
   public static Instance primary;
   public static LinkedList<Task> tasks = new LinkedList<>();

   public static void close () {
      Decraft.trigger(Hook.HookType.CloseStart);
      Decraft.primary.destroy();
      Decraft.primary = null;
      Decraft.trigger(Hook.HookType.CloseEnd);
   }

   public static void open (String root) {
      if (Decraft.engine == null) {
         Decraft.engine = Engine.newBuilder().option("engine.WarnInterpreterOnly", "false").build();
      }
      Decraft.trigger(Hook.HookType.OpenStart);

      Paths.get(root).toFile().mkdir();
      String source = "index.js";
      Config[] configs = {
         new Config(Config.ConfigType.JSON, root, ".decraftrc", false),
         new Config(Config.ConfigType.YAML, root, "config.yml", false),
         new Config(Config.ConfigType.JSON, root, "decraft.json", false),
         new Config(Config.ConfigType.JSON, root, "package.json", true)
      };
      for (Config config : configs) {
         String main = config.getMain();
         if (main != null) {
            source = main;
            break;
         }
      }
      try {
         Decraft.primary = new Instance(Instance.InstanceType.File, root, source, null, null);
         Decraft.primary.open();
      } catch (Throwable error) {
         error.printStackTrace();
      }
      Decraft.trigger(Hook.HookType.OpenEnd);
   }

   public static void tick () {
      Decraft.trigger(Hook.HookType.Tick);
      new LinkedList<>(Decraft.tasks).forEach(task -> task.tick());
   }

   public static void trigger (Hook.HookType type) {
      new LinkedList<>(Decraft.hooks).forEach(hook -> {
         if (hook.type == type) {
            hook.execute();
         }
      });
   }
}
